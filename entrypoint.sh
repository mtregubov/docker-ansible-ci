#!/bin/sh

# Check for GitLab ENTRYPOINT double execution bug (https://gitlab.com/gitlab-org/gitlab-runner/issues/1380)

# Setup ssh key
if [ ! -z "${SSH_PRIV_KEY}" ] && [ ! -f "/root/.ssh/id_rsa" ]; then
    mkdir -p /root/.ssh/
    echo "${SSH_PRIV_KEY}" > /root/.ssh/id_rsa
    chmod -R 600 /root/.ssh/
fi

# Loadkey key to ssh-agent
if [ -f "/root/.ssh/id_rsa" ] && [ ! -f /tmp/ci_runned ]; then
    eval $(ssh-agent)
    ssh-add /root/.ssh/id_rsa
else
    touch /tmp/ci_runned
fi

# Setup kubeconfig
if [ ! -z "${KUBECONFIG_BASE64}" ] && [ ! -f "~/.kube/config" ]; then
    mkdir -p ~/.kube
    echo "${KUBECONFIG_BASE64}" | base64 -d > ~/.kube/config
    chmod 600 ~/.kube/config
fi


# Run default docker entrypoint
exec /usr/local/bin/docker-entrypoint.sh "/bin/bash"
