# Overview
Base Docker image for GitLab CI with docker, ansible and tools.  


# Usage
1. Set secret variable SSH_PRIV_KEY with ssh private key.  
2. Setup a job in `.gitlab-ci.yml`  
   Example:
```
deploy:
  stage: deploy
  image: ${CI_REGISTRY}/path/dind-ansible:version
  services: ['docker:19.03.5-dind']
  script:
    - ansible-playbook deploy.yml
    - docker build .
  when: manual
```
