FROM docker:20.10.22
LABEL maintainer="Andrey Mukhin <andrey@nufl.ru>"

ARG ANSIBLE_VERSION=2.10.7
ARG ANSIBLE_LINT_VERSION=5.0.7
ARG HELM_VERSION=3.10.3
ARG KUBECTL_VERSION=1.26.0
ARG TERRAFORM_VERSION=1.4.5
ENV BASE_URL="https://get.helm.sh"
ENV TAR_FILE="helm-v${HELM_VERSION}-linux-amd64.tar.gz"

RUN apk add --update --no-cache curl ca-certificates bash git gettext jq && \
    curl -sL ${BASE_URL}/${TAR_FILE} | tar -xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    rm -rf linux-amd64


# Install kubectl
RUN curl -sLO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    mv kubectl /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl


RUN apk --update --no-cache add \
      ca-certificates \
      git \
      openssh-client \
      openssl \
      python3 \
      py3-pip \
      rsync \
      curl \
      sshpass \
      bash \
      aws-cli

RUN apk --update add --virtual .build-deps \
      python3-dev \
      libffi-dev \
      openssl-dev \
      build-base \
      gcc \
      cargo \
      musl-dev \
    && \
    pip3 install --upgrade \
      pip \
      cffi \
    && \
    pip3 install \
      ansible==${ANSIBLE_VERSION} \
      ansible-lint==${ANSIBLE_LINT_VERSION} \
      requests \
      pyjwt \
      python-gitlab \
      arrow \
      elasticsearch \
      elasticsearch-dsl \
    && \
    apk del .build-deps && \
    rm -rf /var/cache/apk/*


RUN mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts && \
    echo -e """\
\n\
Host *\n\
    StrictHostKeyChecking no\n\
    UserKnownHostsFile=/dev/null\n\
""" >> /etc/ssh/ssh_config

RUN wget https://hashicorp-releases.mcs.mail.ru/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip

RUN \
	# Unzip
	unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	# Move to local bin
	mv terraform /usr/local/bin/ && \
	# Make it executable
	chmod +x /usr/local/bin/terraform && \
	# Check that it's installed
	terraform --version
  
# RUN echo -e """\
# provider_installation {\n\
#   network_mirror {\n\
#     url = \"https://hub.mcs.mail.ru/repository/terraform-providers/\"\n\
#     include = [\"registry.terraform.io/*/*\"]\n\
#   }\n\
#   direct {\n\
#     exclude = [\"registry.terraform.io/*/*\"]\n\
#     }\n\
#   }\n\
# """ >> ~/.terraformrc


# setup entrypoint
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
